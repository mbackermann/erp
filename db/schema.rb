# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180403193104) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "boletos", force: :cascade do |t|
    t.date "due_date"
    t.date "payment_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "order_id"
    t.decimal "value", precision: 14, scale: 2
    t.index ["order_id"], name: "index_boletos_on_order_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "name"
    t.string "email"
    t.string "phone"
    t.integer "area"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_contacts_on_customer_id"
  end

  create_table "costs", force: :cascade do |t|
    t.string "name"
    t.decimal "value", precision: 14, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cost_type"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.string "razao_social"
    t.string "cnpj"
    t.string "inscricao_municipal"
    t.string "inscricao_estadual"
    t.integer "pj", default: 0
    t.string "email"
    t.string "phone"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "address"
    t.string "zip_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cpf"
    t.string "rg"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string "uid"
    t.datetime "expiry"
  end

  create_table "order_items", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.bigint "order_id"
    t.integer "quantity"
    t.integer "hours"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "value", precision: 14, scale: 2
    t.index ["order_id"], name: "index_order_items_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "customer_id"
    t.date "expiration_date"
    t.text "observations"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "tax", default: true
    t.decimal "total", precision: 14, scale: 2
    t.integer "payment_method"
    t.decimal "hour_price", precision: 14, scale: 2
    t.integer "status"
    t.bigint "user_id"
    t.datetime "confirmed_at"
    t.boolean "dollar", default: false
    t.boolean "show_item_price", default: true
    t.integer "discount"
    t.index ["customer_id"], name: "index_orders_on_customer_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "boletos", "orders"
  add_foreign_key "contacts", "customers"
  add_foreign_key "order_items", "orders"
  add_foreign_key "orders", "customers"
  add_foreign_key "orders", "users"
end
