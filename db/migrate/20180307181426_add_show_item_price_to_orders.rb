class AddShowItemPriceToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :show_item_price, :boolean, default: true
  end
end
