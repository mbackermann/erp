class AddUidAndExpiryToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :uid, :string
    add_column :customers, :expiry, :datetime
  end
end
