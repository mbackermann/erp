class AddTaxToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :tax, :boolean, default: true
  end
end
