class AddValueToOrderItem < ActiveRecord::Migration[5.1]
  def change
    add_column :order_items, :value, :decimal, precision: 14, scale: 2
  end
end
