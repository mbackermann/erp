class AddFieldsToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :cpf, :string
    add_column :customers, :rg, :string
  end
end
