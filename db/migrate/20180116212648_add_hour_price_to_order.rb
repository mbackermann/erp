class AddHourPriceToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :hour_price, :decimal, precision: 14, scale: 2
  end
end
