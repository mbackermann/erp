class RemoveFieldFromCost < ActiveRecord::Migration[5.1]
  def change
    remove_column :costs, :type, :integer
  end
end
