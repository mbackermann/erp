class RemoveAddressNumberFromCustomers < ActiveRecord::Migration[5.1]
  def change
    remove_column :customers, :address_number, :string
  end
end
