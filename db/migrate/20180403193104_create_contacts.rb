class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.references :customer, foreign_key: true
      t.string :name
      t.string :email
      t.string :phone
      t.integer :area

      t.timestamps
    end
  end
end
