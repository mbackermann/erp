class AddValueToBoleto < ActiveRecord::Migration[5.1]
  def change
    add_column :boletos, :value, :decimal, precision: 14, scale: 2
  end
end
