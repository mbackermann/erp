class CreateCosts < ActiveRecord::Migration[5.1]
  def change
    create_table :costs do |t|
      t.string :name
      t.decimal :value, precision: 14, scale: 2
      t.integer :type

      t.timestamps
    end
  end
end
