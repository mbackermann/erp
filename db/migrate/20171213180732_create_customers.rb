class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :razao_social
      t.string :cnpj
      t.string :inscricao_municipal
      t.string :inscricao_estadual
      t.string :cpj
      t.integer :pj, default: 0
      t.string :email
      t.string :phone
      t.string :city
      t.string :state
      t.string :country
      t.string :address
      t.string :address_number
      t.string :zip_code

      t.timestamps
    end
  end
end
