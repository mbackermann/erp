class AddOrderToBoletos < ActiveRecord::Migration[5.1]
  def change
    add_reference :boletos, :order, foreign_key: true
  end
end
