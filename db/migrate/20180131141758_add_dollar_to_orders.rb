class AddDollarToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :dollar, :boolean, default: false
  end
end
