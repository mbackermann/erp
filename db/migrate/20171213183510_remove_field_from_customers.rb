class RemoveFieldFromCustomers < ActiveRecord::Migration[5.1]
  def change
    remove_column :customers, :cpj, :string
  end
end
