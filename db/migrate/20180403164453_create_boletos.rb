class CreateBoletos < ActiveRecord::Migration[5.1]
  def change
    create_table :boletos do |t|
      t.date :due_date
      t.date :payment_date

      t.timestamps
    end
  end
end
