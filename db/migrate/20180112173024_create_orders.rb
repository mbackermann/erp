class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references :customer, foreign_key: true
      t.date :expiration_date
      t.text :observations

      t.timestamps
    end
    execute "SELECT setval('orders_id_seq', 400)"
  end
end
