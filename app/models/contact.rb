class Contact < ApplicationRecord
  belongs_to :customer
  enum area: ["Financeiro", "Marketing", "TI", "Vendas", "Outro"]
  scope :financial, -> {where(area: "Financeiro")}

  validates_presence_of :name, :email, :area
end
