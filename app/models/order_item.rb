class OrderItem < ApplicationRecord
  belongs_to :order
  usar_como_dinheiro :value

  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :name, presence: true
  validates :description, presence: true
  validates :hours, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
