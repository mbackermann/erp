class Cost < ApplicationRecord
  usar_como_dinheiro :value
  enum cost_type: ['Aluguel', 'Salário', 'Despesas de Infraestrutura']

  validates :name, presence: true
  validates :value, presence: true, numericality: { greater_than: 0 }
  validates :cost_type, presence: true
end
