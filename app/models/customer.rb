class Customer < ApplicationRecord
  enum pj: ["pj", "pf"]
  attr_accessor :customer_form
  has_many :orders, dependent: :destroy
  has_many :contacts, dependent: :destroy

  has_attached_file :avatar, styles: { medium: '300x300>', thumb: '100x100>' }, default_url: '/images/:style/missing_company.png'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  validates :name, presence: true
  validates_presence_of :cnpj, :email, :phone, :razao_social, :email, :phone, :city, :state, :country, :zip_code, :address, if: :validate_fields_pj?
  validates_presence_of :cpf, :email, :phone, :city, :state, :country, :zip_code, :address, if: :validate_fields_pf?

  accepts_nested_attributes_for :contacts, reject_if: :all_blank, allow_destroy: true

  def validate_fields_pj?
    customer_form && pj == "pj"
  end

  def validate_fields_pf?
    customer_form && pj == "pf"
  end

  def generate_link!
    self.uid = SecureRandom.uuid
    self.expiry = Time.current + 7.days
    self.save
  end
end
