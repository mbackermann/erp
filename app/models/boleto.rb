class Boleto < ApplicationRecord
  belongs_to :order
  usar_como_dinheiro :value
  scope :next_receipts, -> {where(payment_date: nil).order(:due_date).limit(5)}
end
