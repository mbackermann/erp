class Order < ApplicationRecord
  before_create :set_status
  belongs_to :customer
  belongs_to :user

  has_many :order_items, dependent: :destroy
  has_many :boletos, dependent: :destroy
  usar_como_dinheiro :total, :hour_price
  enum status: ['Cotação', 'Pedido de venda', 'Declinada']

  accepts_nested_attributes_for :order_items, reject_if: :all_blank, allow_destroy: true

  validates :expiration_date, presence: true
  validates :total, presence: true
  validates :payment_method, presence: true
  validate :hourly_cost_minimum

  scope :quotations, -> { where(status: 'Cotação') }
  scope :sales, -> { where(status: 'Pedido de venda')}
  scope :rejected, -> { where(status: 'Declinada')}
  scope :sales_this_month, -> { where(status: 'Pedido de venda', confirmed_at: Time.now.beginning_of_month..Time.now.end_of_month) }

  def hourly_cost_minimum
    if hour_price < Order.hourly_cost
      errors.add(:hour_price, 'não pode ser menor do que o valor mínimo da hora.')
    end
  end

  def self.hourly_cost
    costs = Cost.sum(:value)
    (costs / 160) * 1.1
  end

  def set_status
    self.status = 0
  end
end
