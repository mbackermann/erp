class UsersMailer < ApplicationMailer
  def new_user_mail(user, password)
    @subject = "Cadastro no sistema CASA DE 28"
    @user = user
    @password = password
    @link = unauthenticated_root_url
    mail(to: user.email, subject: 'Bem-vindo ao sistema da CASA DE 28')
  end
end
