class BoletosMailer < ApplicationMailer

  def reminder(boleto, customer)
    @subject = "Você tem um pagamento que vence amanhã"
    @boleto = boleto
    @customer = customer
    if customer.contacts.financial.any?
      email = customer.contacts.financial.collect(&:email).join(",")
    else
      email = customer.email
    end
    mail(to: email, from: 'adm@casade28.com.br', subject: 'Lembrete de Pagamento da CASA DE 28')
  end

end
