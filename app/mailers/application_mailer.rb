class ApplicationMailer < ActionMailer::Base
  include SendGrid
  default from: 'fale@casade28.com.br'
  layout 'mailer'
end
