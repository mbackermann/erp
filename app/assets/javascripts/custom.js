var dollar;
$(document).on("change","input[name='customer[pj]']",function(){
  setCurrentTypeOfCustomer();
})

$(document).on("click",".clickable-row",function(){
  window.document.location = $(this).data("href");
});

$(document).on("click", ".copy-link", function(){
  $(this).parent().parent().find("input").select();
  document.execCommand("Copy");
  $(this).parent().parent().parent().parent().popover('destroy');
  toastr.success('Link copiado com sucesso!');
})

$(document).on('cocoon:after-remove', function(e, insertedItem) {
  $(insertedItem).find(".hidden_value").val("0");
  getTotalMoney();
});

$(document).on("keyup", ".item-hours",function(){
  var hours = $(this).val();
  var price = realToNumber($("#hour-price1").val())
  var item_price = hours*price;
  var field = $(this).parent().parent().find('.item-price');
  var hidden_field = $(this).parent().parent().find(".hidden_value");
  $(field).val(item_price.toFixed(2));
  if(dollar == true){
    VMasker(document.getElementsByClassName("money")).maskMoney({unit: 'U$'});
  }else{
    VMasker(document.getElementsByClassName("money")).maskMoney({unit: 'R$'});
  }
  $(hidden_field).val(item_price.toFixed(2));
  getTotalMoney();
})

$(document).on("keyup", "#hour-price1",function(){
  refreshValue();
})

function realToNumber(real){
  if(dollar == true){
    var decimal = real.split("U$ ")[1].split(",")[1];
    var inteiro = real.split("U$ ")[1].split(",")[0].replace(".","");
  }else{
    var decimal = real.split("R$ ")[1].split(",")[1];
    var inteiro = real.split("R$ ")[1].split(",")[0].replace(".","");
  }
  return number = parseFloat(inteiro+decimal)/100;
}

$(document).on("change", "#order_dollar", function(){
  if($(this).is(":checked")){
    dollar = true;
  }else{
    dollar = false;
  }
  refreshValue();
})


$(document).on("turbolinks:load",function(){

  $('[data-toggle="tooltip"]').tooltip()
  if($("#dollar").length > 0){
    dollar = $("#dollar").data("dollar");
  }
  $('.pt .cnpj').mask('00.000.000/0000-00', {reverse: false});
  $('.en .cnpj').mask('00-0000000', {reverse: false});
  $('.pt .cep').mask('00000-000', {reverse: false});
  $('.cpf').mask('000.000.000-00', {reverse: false});
  var phone_options =  {
    onKeyPress: function(phone, e, field, options) {
      var masks = ['(00) 0000-00009', '(00) 00000-0000'];
      var mask = (phone.length >= 15) ? masks[1] : masks[0];
      $('.pt .phone').mask(mask, options);
    }};

    $('.pt .phone').mask('(00) 0000-00009', phone_options);
    $('.en .phone').mask('(000) 000-0000', phone_options);
    refreshValue();
    getTotalMoney();
  })

  function refreshValue(){
    $(".item-hours").each(function(){
      if(dollar == true){
        VMasker(document.getElementsByClassName("money")).maskMoney({unit: 'U$'});
      }else{
        VMasker(document.getElementsByClassName("money")).maskMoney({unit: 'R$'});
      }
      var hours = $(this).val();
      var price = realToNumber($("#hour-price1").val())
      var item_price = hours*price;
      var field = $(this).parent().parent().find('.item-price');
      var hidden_field = $(this).parent().parent().find(".hidden_value");
      $(field).val(item_price.toFixed(2));
      if(dollar == true){
        VMasker(document.getElementsByClassName("money")).maskMoney({unit: 'U$'});
      }else{
        VMasker(document.getElementsByClassName("money")).maskMoney({unit: 'R$'});
      }
      $(hidden_field).val(item_price.toFixed(2));
    });
    getTotalMoney();
  }

  function getTotalMoney(){

    var total = 0;
    $(".hidden_value").each(function(){
      total += parseFloat($(this).val());
    });
    if($("#order_tax").is(":checked")){
      total = total*1.09
    }
    var discount = (100-parseInt($("#order_discount").val(),10))/100;
    console.log(discount);
    $("#total-order").val(numberToReal(total*discount));
  }

  $(document).on("change","#order_discount",function(){
    getTotalMoney();
  })

  $(document).on("change","#order_tax",function(){
    getTotalMoney();
  });

  function numberToReal(numero) {
    var numero = numero.toFixed(2).split('.');
    if(dollar == true){
      numero[0] = "U$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    }else{
      numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    }
    return numero.join(',');
  }

  $(document).on("turbolinks:load",function(){
    if($('.money').length > 0){
      if(dollar == true){
        VMasker(document.getElementsByClassName("money")).maskMoney({unit: 'U$'});
      }else{
        VMasker(document.getElementsByClassName("money")).maskMoney({unit: 'R$'});
      }
    }
  })

  function setCurrentTypeOfCustomer(){
    if($($("input[name='customer[pj]']")[1]).is(":checked")){
      showPjFields();
    }else{
      showPfFields();
    }
  }

  function showPjFields(){
    $("#pj_fields").show();
    $("#pf_fields").hide();
    $("#pf_fields input").each(function(){
      $(this).val("");
    })
  }

  function showPfFields(){
    $("#pj_fields").hide();
    $("#pf_fields").show();
    $("#pj_fields input").each(function(){
      $(this).val("");
    })
  }

  $(document).on("turbolinks:load",function(){
    setCurrentTypeOfCustomer();
    countrySelect();
  })

  // Select Country, State and City

  $(document).on("change", "#country_select", function() {
    countrySelect();
  });

  function countrySelect(){
    var country = $("#country_select").val();
    if(country != ""){
      var input_country = $(this);
      var input_state = $("#state_select");
      var input_city = $("#city_select");
      $.ajax({
        url: '/states/'+country,
        method: 'get',
        success: function(data){
          input_state.empty();
          input_city.empty();
          var opt = '<option></option>';
          input_state.append(opt);
          $.each(data, function (key,value) {
            var opt = '<option value='+ key +'>' + value + '</option>';
            input_state.append(opt);
          });

          if($("#state_select").data("state") != null){
            var state = $("#state_select").data("state");
            $("#state_select").val(state);
            stateSelect();
          }

        }
      })
    }
  }

  function stateSelect(){
    var country = $("#country_select").val();
    var state = $("#state_select").val();
    if(state != ""){
      var input_state = $(this);
      var input_city = $("#city_select");
      var url = '/cities/'+country+'/'+state;
      $.ajax({
        url: url,
        method: 'get',
        success: function(data){
          input_city.empty();
          var opt = '<option></option>';
          input_city.append(opt);
          $.each(data, function (key,value) {
            var opt = '<option value="'+ value +'">' + value + '</option>';
            input_city.append(opt);
          });

          if($("#city_select").data("city") != null){
            var city = $("#city_select").data("city");
            $("#city_select").val(city);
          }

        }
      })
    }
  }

  $(document).on("change", "#state_select", function() {
    stateSelect();

  });


  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('.avatar').css("background-image", "url("+e.target.result+")");
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $(document).on('change','#input_avatar',function() {
    readURL(this);
  });
