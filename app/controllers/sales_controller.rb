class SalesController < ApplicationController
  before_action :set_sale, only: %w(show destroy generate_boletos destroy_boletos)

  def index
    @orders = Order.sales.order('id DESC')
  end

  def show
  end

  def destroy
    @order.destroy
    respond_with @order, location: orders_path
  end

  def generate_boletos
    if @order.payment_method.nil? || @order.status != 'Pedido de venda'
      redirect_to order_path(@order)
    end
    if params[:date].blank?
      flash[:alert] = 'Favor preencher a data do primeiro boleto'
      render 'sales/show'
      return
    end
    date = Date.parse(params[:date])
    value = @order.total/@order.payment_method
    for i in 0..(@order.payment_method-1)
      @boleto = @order.boletos.build
      @boleto.due_date = date + i.months
      @boleto.value = value
      @boleto.save
    end
    flash[:notice] = "Boletos criados com sucesso!"
    redirect_to sale_path(@order)
  end

  def destroy_boletos
    if @order.boletos.destroy_all
      flash[:notice] = "Boletos apagado com sucesso!"
    else
      flash[:alert] = "Erro ao apagar os boletos!"
    end
    redirect_to sale_path(@order)
  end

  private

  def set_sale
    @order = Order.find_by(id: params[:id])
    if @order.status != "Pedido de venda"
      flash[:alert] = "Ação apenas disponível para pedidos de venda!"
      redirect_to sales_path
    end
  end

end
