class CostsController < AdminController
  before_action :set_cost, only: %w[edit destroy update]

  def index
    @costs = Cost.all.order(:name)
  end

  def new
    @cost = Cost.new
    @cost.value ||= 0
  end

  def create
    @cost = Cost.create(cost_params)
    respond_with @cost, location: costs_path
  end

  def edit; end

  def destroy
    @cost.destroy
    respond_with @cost, location: costs_path
  end

  def update
    @cost.update_attributes(cost_params)
    respond_with @cost, location: costs_path
  end

  private

  def set_cost
    @cost = Cost.find_by(id: params[:id])
  end

  def cost_params
    params
      .require(:cost)
      .permit(:name, :value, :cost_type)
  end
end
