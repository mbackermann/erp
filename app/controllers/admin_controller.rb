class AdminController < ApplicationController
  before_action :authenticate_user!, except: %w(states cities)

  def states
    render json: CS.states(params[:country]).to_json
  end

  def cities
    render json: CS.cities(params[:state], params[:country]).to_json
  end
end
