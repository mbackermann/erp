class BoletosController < AdminController
  before_action :set_boleto, only: %w(destroy edit update)

  def destroy
    @boleto.destroy
    respond_with @boleto, location: sale_path(@order)
  end

  def update
    @boleto.update_attributes(boleto_params)
    respond_with @boleto, location: sale_path(@order)
  end

  def edit
    respond_to do |format|
      format.js {}
    end
  end

  def new
    @boleto = Order.find_by(id: params[:sale_id]).boletos.build
    respond_to do |format|
      format.js {}
    end
  end

  def create
    order = Order.find_by(id: params[:sale_id])
    @boleto = order.boletos.build
    @boleto.attributes = boleto_params
    if @boleto.save
      flash[:notice] = 'Boleto criado com sucesso!'
    else
      flash[:alert] = 'Erro ao criar boleto!'
    end
    redirect_to sale_path(order)
  end


  private
  def set_boleto
    @order = Order.find_by(id: params[:sale_id])
    @boleto = Boleto.find_by(id: params[:id])
  end

  def boleto_params
    params
      .require(:boleto)
      .permit(:value, :due_date, :payment_date)
  end

end
