class OrdersController < AdminController
  before_action :set_hourly, only: %w[new create edit update]
  before_action :set_order, only: %w[edit update destroy show confirm_sale reject_sale]

  def new
    @order = Order.new
    @order.hour_price = @hourly
    @order.order_items.build
  end

  def create
    @order = Order.create(order_params)
    respond_with @order, location: orders_path
  end

  def all_orders
    @orders = Order.all
    respond_to do |format|
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"pedidos.csv\""
        headers['Content-Type'] ||= 'text/csv;charset=UTF-8'
      end
      format.xls do
        headers['Content-Disposition'] = "attachment; filename=\"pedidos.xls\""
        headers['Content-Type'] ||= 'text/xls;charset=UTF-8'
      end
    end
  end

  def index
    @orders = Order.quotations.order('id DESC')
    @rejected = Order.rejected.order('id DESC')
  end

  def confirm_sale
    @order.update_attributes(status: 'Pedido de venda', confirmed_at: Time.current)
    respond_with @sale, location: orders_path
  end

  def reject_sale
    @order.update_attributes(status: 'Declinada')
    respond_with @order, location: orders_path
  end

  def edit; end

  def update
    @order.update_attributes(order_params)
    respond_with @order, location: orders_path
  end

  def destroy
    @order.destroy
    respond_with @order, location: orders_path
  end

  def show
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "SO#{@order.id} - #{@order.customer.name}",
               template: 'orders/order.html.erb',
               layout: 'layouts/pdf',
               disposition: 'attachment',
               margin: {
                 top: 20,
                 bottom: 20
               },
               show_as_html: params.key?('debug'),
               header: {
                 html: { template: 'layouts/pdf_header.html.erb' },
                 spacing: 0
               },
               footer: {
                 html: { template: 'layouts/pdf_footer.html.erb' },
                 spacing: 0
               }
      end
    end
  end

  private

  def order_params
    params
      .require(:order)
      .permit(:hour_price, :dollar, :user_id, :customer_id, :expiration_date, :observations, :tax, :total, :payment_method, :show_item_price, :discount, order_items_attributes: %i[id name description hours quantity value _destroy])
  end

  def set_order
    @order = Order.includes(:customer).find_by(id: params[:id])
    if @order.status == "Pedido de venda"
      flash[:alert] = "Essa SO já virou uma venda!"
      redirect_to orders_path
    end
  end

  def set_hourly
    @hourly = Order.hourly_cost
  end
end
