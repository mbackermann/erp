require 'application_responder'

class ApplicationController < ActionController::Base
  before_action :set_locale
  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery with: :exception

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options
    { :locale => ((I18n.locale == I18n.default_locale) ? nil : I18n.locale) }
  end

end
