class DashboardController < AdminController

  def show
    @sales = Order.sales_this_month.order(:confirmed_at)
    @customers = Customer.all.order(:name)
    @receipts = Boleto.next_receipts
  end

end
