class UsersController < AdminController
  before_action :set_user, only: %w[show edit update destroy]

  def index
    @users = User.all.order(:name)
  end

  def show; end

  def edit; end

  def update
    @user.update_attributes(user_params)
    respond_with @user, location: users_path
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    password = @user.generate_password
    @user.password = password
    if @user.save
      UsersMailer.new_user_mail(@user, password).deliver_now
      flash[:notice] = 'Usuário criado com sucesso!'
      redirect_to users_path
    else
      flash[:alert] = 'Erro ao adicionar usuário!'
      render :new
    end
  end

  def destroy; end

  def profile
    @user = current_user
  end

  def update_password
    @user = current_user
    if @user.update_with_password(user_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      flash[:notice] = 'Senha alterada com sucesso!'
      redirect_to profile_users_path
    else
      flash[:alert] = 'Erro ao salvar sua senha'
      render 'profile'
    end
  end

  private

  def set_user
    @user = User.find_by(id: params[:id])
  end

  def user_params
    params
      .require(:user)
      .permit(:name, :email, :password, :password_confirmation, :current_password)
  end
end
