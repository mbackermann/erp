class CustomersController < ApplicationController
  before_action :set_customer, only: %w[show update destroy ]
  before_action :authenticate_user!, except: %w[edit update]

  def index
    @customers = Customer.all.order(:name)
  end

  def new
    @customer = Customer.new
  end

  def create
    @customer = Customer.create(customer_params)
    respond_with @customer, location: customers_path
  end

  def generate_link
    respond_to do |format|
      format.js {
        @customer = Customer.find_by(id: params[:customer_id])
        if @customer.expiry.nil? || @customer.expiry < Time.current
          @customer.generate_link!
        end
        @link = edit_customer_url(@customer.uid)
      }
    end
  end

  def edit
    if user_signed_in?
      @customer = Customer.find_by(id: params[:id])
    else
      @customer = Customer.where("uid = ? AND expiry > ?", params[:id], Time.current).first
    end
  end

  def update
    if user_signed_in?
      @customer.update_attributes(customer_params)
      respond_with @customer, location: customers_path
    else
      c_params = customer_params
      c_params[:customer_form] = true
      if @customer.update_attributes(c_params)
        flash[:notice] = "Seus dados foram salvos com sucesso!"
        redirect_to edit_customer_path(@customer.uid)
      else
        flash[:alert] = "Erro ao salvar seus dados!"
        render :edit
      end
    end
  end

  def destroy
    @customer.destroy
    respond_with @customer, location: customers_path
  end

  def show; end

  private

  def set_customer
    @customer = Customer.find_by(id: params[:id])
  end

  def customer_params
    params
    .require(:customer)
    .permit(:avatar, :pj, :name, :razao_social, :inscricao_municipal,
      :inscricao_estadual, :zip_code, :cnpj, :cpf, :rg, :country, :city, :state,
      :address, :phone, :email, contacts_attributes: %i[id name phone email area _destroy])
    end
  end
