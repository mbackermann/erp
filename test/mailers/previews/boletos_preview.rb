class BoletosPreview < ActionMailer::Preview

  def reminder
    BoletosMailer.reminder(Boleto.first, Boleto.first.order.customer)
  end

end
