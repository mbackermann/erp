Rails.application.routes.draw do
  scope "(:locale)", locale: /pt|en/ do
    devise_for :users

    get 'states/:country' => 'admin#states'
    get 'cities/:country/:state' => 'admin#cities'
    get 'listapedidos' => 'orders#all_orders', :as => :all_orders

    resources :customers do
      patch 'generate_link'
    end
    resources :costs
    resources :orders do
      member do
        patch '/confirm_sale' => 'orders#confirm_sale', as: :confirm_sale
        patch '/reject_sale' => 'orders#reject_sale', as: :reject_sale
      end
    end
    resources :sales, only: %w(index show destroy) do
      member do
        post '/generate_boletos' => 'sales#generate_boletos', as: :generate_boletos
        delete '/destroy_boletos' => 'sales#destroy_boletos', as: :destroy_boletos
      end
      resources :boletos, except: %w(index show)
    end
    resources :users do
      collection do
        get '/perfil' => 'users#profile', as: :profile
        patch 'update_password'
      end
    end

    devise_scope :user do
      authenticated :user do
        root to: 'dashboard#show', as: :authenticated_root
      end

      unauthenticated :user do
        root to: 'devise/sessions#new', as: :unauthenticated_root
      end
    end
  end
end
