require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Erp
  class Application < Rails::Application
    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.i18n.available_locales = [:pt, :en]

    config.i18n.default_locale = :pt
    config.time_zone = 'Brasilia'
    config.active_record.default_timezone

    ActionMailer::Base.smtp_settings = {
      address: 'smtp.sendgrid.net',
      port: 25,
      domain: 'agenciaflin.com.br',
      authentication: :plain,
      user_name: 'agenciaflin',
      password: 'Flin2373!'
    }

    config.paperclip_defaults = {
      storage: :s3,
      s3_credentials: {
        bucket: 'flin-cbdi2018',
        access_key_id: 'AKIAIT53DXIUBGPLPPHA',
        secret_access_key: '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',
        s3_region: 'sa-east-1',
        s3_host_name: 's3-sa-east-1.amazonaws.com'
      }
    }
  end
end
